# Test for Python/Django Developers

This is a simple application of a Tutor Match.

## Development

Build the containers with:

- `docker-compose build`

Apply database migrations with:

- `docker-compose run webapp pipenv run python3 manage.py migrate`

Create the admin user with:

- `docker-compose run webapp pipenv run python3 manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')"`

Providing initial data for models

- `docker-compose run webapp pipenv run python3 manage.py loaddata initial`

Spin up the containers with:

- `docker-compose -f docker-compose.yml -f docker-compose.dev.yml up`

Acess the main webapp at `localhost:8000`

## Running Tests

- `docker-compose run webapp pipenv run python3 manage.py test`

## Examples

- Create a tutor:
```
curl -X POST -H "Content-Type: application/json" -d '{ "name": "Tutor 4", "gender": "male", "email": "tutor4@test.com", "phone": "999-999-9999"}' http://localhost:8000/tutors/
Resposta: HTTP 201
    {
        "name": "Tutor 4",
        "gender": "male",
        "email": "tutor4@test.com",
        "phone": "999-999-9999"
    }
```

- List tutors:
```
curl -X GET http://localhost:8000/tutors/
Resposta: HTTP 200
[
    {
        "name": "Tutor 1",
        "gender": "male",
        "email": "tutor1@test.com",
        "phone": "777-777-7777"
    },
    {
        "name": "Tutor 2",
        "gender": "female",
        "email": "tutor2@test.com",
        "phone": "888-888-8888"
    },
    {
        "name": "Tutor 3",
        "gender": "female",
        "email": "tutor3@test.com",
        "phone": "999-999-9999"
    }
]
```

- Delete tutor
```
curl -X DELETE http://localhost:8000/tutors/1/
Resposta: HTTP 204
```
