from django.test import TestCase
from django.urls import reverse

from .models import Tutor


class TutorModelTest(TestCase):
    """Tutor Model Test"""

    @classmethod
    def setUpTestData(cls):
        """Set up non-modified objects used by all test methods"""
        cls.tutor = Tutor.objects.create(
            name='Test Tutor 1',
            gender='male',
            email='test1@test.com',
            phone='111-111-1111'
        )

    def test_name_label(self):
        """Test name label"""
        tutor = Tutor.objects.get(id=1)
        field_label = tutor._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'name')

    def test_gender_value(self):
        """Test Gender Value"""
        tutor = Tutor.objects.get(id=1)
        field_value = tutor.gender
        self.assertEquals(field_value, 'male')

    def test_name_max_length(self):
        """Test Name Max Length"""
        tutor = Tutor.objects.get(id=1)
        max_length = tutor._meta.get_field('name').max_length
        self.assertEquals(max_length, 200)

    def test_view_url_exists_at_desired_location(self):
        """Test view url exists"""
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)
