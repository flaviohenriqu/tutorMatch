""" Tutor Serializer """
from rest_framework import serializers
from .models import Tutor


class TutorSerializer(serializers.ModelSerializer):
    """ Tutor Serializer """

    class Meta:
        model = Tutor
        fields = ('name', 'gender', 'email', 'phone')
