""" Models Tutor """
from django.db import models


class Tutor(models.Model):
    """ Tutor Model """
    MALE, FEMALE = 'male', 'female'
    GENDERS = (
        (MALE, 'male'),
        (FEMALE, 'female')
    )

    name = models.CharField(max_length=200)
    gender = models.CharField(
        max_length=6,
        choices=GENDERS,
        default=MALE
    )
    phone = models.CharField(max_length=60, null=True)
    email = models.CharField(max_length=127)
