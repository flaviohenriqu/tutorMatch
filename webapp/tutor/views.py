""" Tutor Views """
from django.shortcuts import render

from rest_framework import viewsets

from .models import Tutor
from .serializers import TutorSerializer


class TutorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows tutors to be viewed or edited.
    """
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer
